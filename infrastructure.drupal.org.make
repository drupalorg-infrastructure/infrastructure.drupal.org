core = 7.x
api = 2
projects[drupal][version] = "7.34"


; Modules
projects[bueditor][version] = "1.8"

projects[ckeditor][version] = "1.16"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][destination] = "modules"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.4/ckeditor_4.4.4_standard.zip"
libraries[ckeditor][directory_name] = "ckeditor/ckeditor"

projects[ctools][version] = "1.6"
projects[codefilter][version] = "1.1"
projects[content_access][version] = "1.2-beta2"
projects[date][version] = "2.8"
projects[diff][version] = "3.2"
projects[entity][version] = "1.6"
projects[entityreference][version] = "1.1"
projects[features][version] = "2.3"
projects[field_extrawidgets][version] = "1.1"
projects[field_group][version] = "1.4"
projects[google_analytics][version] = "2.1"
projects[link][version] = "1.3"
projects[logintoboggan][version] = "1.4"
projects[pathauto][version] = "1.2"
projects[redirect][version] = "1.0-rc1"
projects[r4032login][version] = "1.8"
projects[token][version] = "1.5"
projects[views][version] = "3.10"


; Custom modules
projects[infrastructure_drupalorg][version] = "1.x-dev"
projects[infrastructure_drupalorg][download][revision] = "bf92d72"


;; Common for Drupal.org D7 sites.
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"


; Responsive theme!
projects[bluecheese][download][branch] = "branded-2.x"
