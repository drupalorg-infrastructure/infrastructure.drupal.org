<?php

$update_free_access = FALSE;
$drupal_hash_salt = '';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

// Drupal.org settings.
$conf['drupalorg_site'] = 'infrastructure';
$conf['drupalorg_crosssite_lock_permissions'] = TRUE;
$conf['logintoboggan_pre_auth_role'] = 8;
$conf['drupalorg_crosssite_trusted_role'] = 13;
$conf['drupalorg_crosssite_community_role'] = 18;

// Disable poormanscron.
$conf['cron_safe_threshold'] = 0;

// We don't use link theming.
$conf['theme_link'] = FALSE;

// Include a common settings file if it exists.
$common_settings = '/var/www/settings.common.php';
if (file_exists($common_settings)) {
  include $common_settings;
}

// Include a local settings file if it exists.
$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}
